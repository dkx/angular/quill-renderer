import {NgModule} from '@angular/core';

import {QuillRendererComponent} from './quill-renderer.component';


@NgModule({
	declarations: [
		QuillRendererComponent,
	],
	exports: [
		QuillRendererComponent,
	],
})
export class QuillRendererModule {}
