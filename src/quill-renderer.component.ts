import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {QuillDeltaToHtmlConverter} from 'quill-delta-to-html';


@Component({
	selector: 'dkx-quill-renderer',
	template: '',
	host: {
		'[innerHtml]': 'html',
	},
})
export class QuillRendererComponent implements OnChanges
{


	@Input()
	public delta: any;

	public html: string = '';


	public ngOnChanges(changes: SimpleChanges): void
	{
		if (typeof changes.delta !== 'undefined') {
			const converter = new QuillDeltaToHtmlConverter(changes.delta.currentValue.ops);
			this.html = converter.convert();
		}
	}

}
